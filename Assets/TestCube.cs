﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

using static UnityEngine.Color;
public class TestCube : MonoBehaviour {
    public bool drawRR, drawRU, drawRF, drawUU, drawUR, drawUF, drawFF, drawFR, drawFU;
    Vector3 pos;
    private void OnDrawGizmos()
    {

         pos = transform.position;
        Vector3 right = transform.right, up = transform.up, forward = transform.forward;
        #region Axis
        Gizmos.color = red;
        Draw(right);
        Gizmos.color = green;
        Draw(up);
        Gizmos.color = blue;
        Draw(forward);
        #endregion 
        Handles.color = red;
        Handles.DrawWireDisc(pos, Vector3.right, 1);
        Handles.color = green;
        Handles.DrawWireDisc(pos, Vector3.up, 1);
        Handles.color = blue;
        Handles.DrawWireDisc(pos, Vector3.forward, 1);


        Vector3 uptoUp = Vector3.ProjectOnPlane(up, Vector3.up).normalized;
        if (drawUU)Draw(uptoUp, yellow);
        Vector3 uptoRight = Vector3.ProjectOnPlane(up, Vector3.right).normalized;
        if (drawUR)Draw(uptoRight, yellow);
        Vector3 uptoForward = Vector3.ProjectOnPlane(up, Vector3.forward).normalized;
        if(drawUF)Draw(uptoForward, yellow);


        Vector3 rightToRight = Vector3.ProjectOnPlane(right, Vector3.right).normalized;
        if (drawRR) Draw(rightToRight, magenta);
        Vector3 righttoUp = Vector3.ProjectOnPlane(right, Vector3.up).normalized;
        if (drawRU) Draw(righttoUp, magenta);
        Vector3 righttoForward = Vector3.ProjectOnPlane(right, Vector3.forward).normalized;
        if (drawRF) Draw(righttoForward, magenta);


        Vector3 forwardToforward = Vector3.ProjectOnPlane(forward, Vector3.forward).normalized;
        if (drawFF) Draw(forwardToforward, cyan);
        Vector3 forwardToRight = Vector3.ProjectOnPlane(forward, Vector3.right).normalized;
        if (drawFR) Draw(forwardToRight, cyan);
        Vector3 forwardToUp = Vector3.ProjectOnPlane(forward, Vector3.up).normalized;
        if (drawFU) Draw(forwardToUp, cyan);


        float angleU = Vector3.Angle(Vector3.up, uptoRight);
        float angleF = Vector3.Angle(Vector3.forward, forwardToRight);
        float angle =angleU*Mathf.Sin(angleF/180*Mathf.PI) ;


        
        Handles.Label(pos,angle.ToString());
    }

    private  void Draw( Vector3 right)
    {
        Gizmos.DrawLine(pos, pos + right);
    }
    private void Draw(Vector3 right, Color color)
    {
        Gizmos.color = color;
        Gizmos.DrawLine(pos, pos + right);
    }
}
#endif