﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GloveValues;
using UnityEngine.UI;
using System;
using Cynteract.PlayerInput;
using UnityEngine.Video;

public delegate void callBackVoid();
public class Calibration : MonoBehaviour {
    delegate void ButtonPressedActions();
    public VideoPlayer thumb, fingers;
    public Text text;
    private int buttonIndex;
    public Button button;
    ButtonPressedActions[] buttonPressedActions;
    callBackVoid callBack;
    public GameObject image;
    private bool breakUpdating;
    KeyBind keyBind;
    KeyBind keyBind2;
    private bool interrupted;
    public enum Mode { Fingers, Thumb};
    private Mode mode;
    public void Calibrate(Mode mode,callBackVoid callback)
    {
        this.mode = mode;
        button.gameObject.SetActive(false);
        GlobalValues.cannonControl.Freeze();
        interrupted = false;
        keyBind = new KeyBind(KeyCode.Return, ButtonPressed, Input.GetKeyDown);
        keyBind2 = new KeyBind(KeyCode.Space, ButtonPressed, Input.GetKeyDown);
        GlobalValues.playerInput.callbackKey(keyBind, keyBind2);
        this.callBack = callback;
        buttonIndex = 0;
        buttonPressedActions = new ButtonPressedActions[] { measureFinishedWindow };
        measureBothWindow();
    }
    void measureBothWindow()
    {
        if (mode==Mode.Fingers)
        {
            thumb.gameObject.SetActive(false);
            fingers.gameObject.SetActive(true);
            printM("Bitte öffnen und schließen Sie Ihre Hand so weit wie möglich");

        }
        else if (mode == Mode.Thumb)
        {
            thumb.gameObject.SetActive(true);
            fingers.gameObject.SetActive(false);
            printM("Bitte öffnen und schließen Sie den Daumen so weit wie möglich");

        }
        StartCoroutine(measureBoth());

    }
    IEnumerator measureBoth()
    {
        yield return new WaitUntil(GloveScript.calibSC.recievedUpdate);
        GloveScript.gloveInterpreter.StartCalibrating();
        StartCoroutine(pressButtonAfterTime(10));
    }
    void stopMeasuringBoth()
    {
        GloveScript.gloveInterpreter.StopCalibrating();
    }
    IEnumerator updateMaxValues()
    {
        while (!breakUpdating)
        {
            GlobalValues.maxAngle = Mathf.Max(GlobalValues.maxAngle, GlobalValues.angle);
            GlobalValues.minAngle = Mathf.Min(GlobalValues.minAngle, GlobalValues.angle);
            yield return null;
        }
        breakUpdating = false;
    }
 
    
    void measureFinishedWindow()
    {
        GloveScript.gloveInterpreter.StopCalibrating();
        breakUpdating = true;
        image.gameObject.SetActive(false);
        button.gameObject.SetActive(false);
        printM("Vielen Dank!");
        GlobalValues.playerInput.stopCallBackKey(keyBind, keyBind2);
        StartCoroutine(DisableCalibration());
    }





    private IEnumerator DisableCalibration()
    {
        yield return new WaitForSecondsRealtime(1);

        callBack();
        GlobalValues.cannonControl.Unfreeze();

        gameObject.SetActive(false);
    }
    


    private void printM(string v)
    {
        text.text = v;
    }

    private void printMAdd(string v, FontStyle fontStyle)
    {
        text.text += v;
    }



    public void ButtonPressed()
    {
        if (buttonIndex< buttonPressedActions.Length)
        {
            buttonPressedActions[buttonIndex]();
            buttonIndex++;
        }

    }
    IEnumerator pressButtonAfterTime(int time)
    {
        //button.gameObject.SetActive(true);
        button.gameObject.SetActive(true);

        Text text = button.GetComponentInChildren<Text>();
        for (int i = 0; i < time; i++)
        {
            text.text = "Weiter (" + (time - i) + ")";

            yield return new WaitForSeconds(1);
            if (interrupted)
            {
                break;
            }
        }

        if (interrupted)
        {
            interrupted = false;

        }
        else
        {
            ButtonPressed();
        }
    }
}
