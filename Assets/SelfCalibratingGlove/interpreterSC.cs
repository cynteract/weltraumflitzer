﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace GloveValues
{
    public class interpreterSC : MonoBehaviour
    {

        static readonly int angleNumber = 14;



        #region settings
        [Header("Settings")]
        public IMUMeasureMode imuMeasureMode;
        public ForceMeasureMode forceMeasureMode;
        public FingerMeasureMode fingerMeasureMode;
        public bool IMUnotForce;
        public bool[] fingersToCare = new bool[5];
        [Range(0, 1)]
        public float upperThreashhold, lowerThreashhold;

        internal void SubscribeInterpretedData(InterpretedDataCallback callback)
        {
            callbacks.Add(callback);
        }
        #endregion
        #region Private Variables

        private float[] imuAngles = new float[angleNumber];

        List<InterpretedDataCallback> callbacks = new List<InterpretedDataCallback>();

        private float[]
        upperValues = new float[angleNumber],
        lowerValues = new float[angleNumber],
        middleThreashholds = new float[angleNumber],
        closedThreashholds = new float[angleNumber],
        openThreashholds = new float[angleNumber];
        private float forceAverage;
        private float imuAverage;
        #endregion
        [Header("Values")]
        public bool closed;
        public bool[] fingersClosed=new bool [5];
        [Header("Averages")]
        public float average;
        public float upperAverage, lowerAverage, middleAverage, closedAverage, openAverage;
        private float[] forceValues = new float[5];
        private void Start()
        {
            GloveScript.calibSC.SubscribeAngles(callback);
        }
        private void callback(float[] angles, float[] forces)
        {
            imuAverage = 0;
            for (int i = 0; i < angles.Length; i++)
            {
                imuAngles[i] = angles[i];
                imuAverage += imuAngles[i];
            }
            interpreteData();
            foreach (var item in callbacks)
            {
                item(closed);
            }
        }
        private void Awake()
        {
            GloveScript.gloveInterpreter = this;
        }
        private void interpreteData()
        {

            if (IMUnotForce)
            {
                average = imuAverage;
                InterpretingFunctions.interprIMUData(
                    ref closed,
                    imuMeasureMode,
                    imuAverage,
                    imuAngles,
                    middleThreashholds,
                    closedThreashholds,
                    openThreashholds,
                    fingersToCare);
            }
            else
            {
                average = forceAverage;
                InterpretingFunctions.interprForceData(ref closed, forceMeasureMode, forceAverage);
            }

            InterpretingFunctions.fingersClosed(ref fingersClosed, imuAngles, middleThreashholds, closedThreashholds, openThreashholds, fingerMeasureMode);
        }
        #region calibration
        bool breakMax = false, breakMin = false;


        public void calibrateMax()
        {

            StartCoroutine(calibratingMax());

        }
        public void stopCalibratingMax()
        {
            StopCoroutine(calibratingMax());
            breakMax = true;
        }
        public void calibrateMin()
        {
            StartCoroutine(calibratingMin());
        }
        public void stopCalibratingMin()
        {
            StopCoroutine(calibratingMin());
            breakMin = true;
        }
        public void StartCalibrating()
        {
            StartCoroutine(calibratingMax());
            StartCoroutine(calibratingMin());
        }

        public void StopCalibrating()
        {
            StopCoroutine(calibratingMax());
            StopCoroutine(calibratingMin());
            breakMax = true;
            breakMin = true;
        }
        IEnumerator calibratingMax()
        {
            float[] maxAngle = new float[angleNumber];
            for (int i = 0; i < maxAngle.Length; i++)
            {
                maxAngle[i] = 0;
            }
            while (true)
            {
                for (int i = 0; i < maxAngle.Length; i++)
                {
                    if (imuAngles[i] > maxAngle[i])
                    {
                        maxAngle[i] = imuAngles[i];
                    }
                }
                if (breakMax)
                {
                    breakMax = false;
                    break;
                }
                setUpperValues(maxAngle);
                yield return null;
            }
        }
        IEnumerator calibratingMin()
        {
            float[] minAngle = new float[angleNumber];
            for (int i = 0; i < minAngle.Length; i++)
            {
                minAngle[i] = 360;
            }
            while (true)
            {
                for (int i = 0; i < minAngle.Length; i++)
                {
                    if (imuAngles[i] < minAngle[i])
                    {
                        minAngle[i] = imuAngles[i];

                    }
                }
                if (breakMin)
                {
                    breakMin = false;
                    break;
                }
                setLowerValues(minAngle);
                yield return null;
            }
        }
        public void setUpperValues(float[] values)
        {
            middleAverage = 0;
            upperAverage = 0;
            closedAverage = 0;
            openAverage = 0;
            for (int i = 0; i < upperValues.Length; i++)
            {
                upperValues[i] = values[i];
                middleThreashholds[i] = (upperValues[i] + lowerValues[i]) / 2;
                closedThreashholds[i] = (upperValues[i] + lowerValues[i]) * upperThreashhold;
                openThreashholds[i] = (upperValues[i] + lowerValues[i]) * lowerThreashhold;


                middleAverage += middleThreashholds[i];
                upperAverage += upperValues[i];
                closedAverage += closedThreashholds[i];
                openAverage += openThreashholds[i];
            }

        }

        public void setLowerValues(float[] values)
        {
            lowerAverage = 0;
            for (int i = 0; i < lowerValues.Length; i++)
            {
                lowerValues[i] = values[i];
                lowerAverage += lowerValues[i];
            }
        }
        #endregion
    }


#if UNITY_EDITOR
    [CustomEditor(typeof(interpreterSC))]
    public class interpreterSCEditor: Editor
    {
        bool calibrating = false;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            interpreterSC interpreterSC =(interpreterSC) target;

            if (Application.isPlaying)
            {
                if (!calibrating)
                {
                    if (GUILayout.Button("Calibrate"))
                    {
                        calibrating = true;
                        interpreterSC.StartCalibrating();
                    }
                }
                else
                {
                    if (GUILayout.Button("Stop Calibrating"))
                    {
                        calibrating = false;
                        interpreterSC.StopCalibrating();
                    }
                }
            }

        }
    }
#endif
}