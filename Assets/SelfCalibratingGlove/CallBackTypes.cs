﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GloveValues
{
    public delegate void HandValuesCallback(float[] values, Quaternion armRotation, Quaternion wristRotation, Quaternion[] fingerRotations);
    public delegate void GloveAnglesCallback(float[] angles, float[]forces);
    public delegate void InterpretedDataCallback(bool closed);
    public delegate bool compare(float a, float b);
    public enum IMUMeasureMode { Average, AverageTwoThreashholds, OneThreashhold, OpenAndCloseThreashholds, FingerAverage, FingeraverageTwoThreashholds };
    public enum ForceMeasureMode { Average, AverageTwoThreashholds, Finger, FingerTwoThreasHolds };
    public enum FingerMeasureMode { Average, AverageTwoThreashholds };
}


