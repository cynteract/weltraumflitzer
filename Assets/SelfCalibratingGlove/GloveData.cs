﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace GloveValues
{
    public class GloveData : MonoBehaviour
    {
        
        
        List<HandValuesCallback> callBacks = new List<HandValuesCallback>();
        
        public DateTime lastTime;
        public float timeToReconnect;

        public Quaternion armRotation { get; private set; }
        public DatabaseScript Database;
        static readonly string[] DMS_Paths ={
        "Sensors/DMS_Daumen",
        "Sensors/DMS_Zeige" ,
        "Sensors/DMS_Mittel",
        "Sensors/DMS_Ring"  ,
        "Sensors/DMS_Klein",

        };
        static readonly string wristPath = "Sensors/Hand";
        private string armPath= "Sensors/Arm";
        static readonly string[] fingerPaths =
        {
          "Sensors/Daumen_S3", "Sensors/Daumen_S1", "Sensors/Daumen_S2",
          "Sensors/Zeige_S1","Sensors/Zeige_S2", "Sensors/Zeige_S3",
          "Sensors/Mittel_S1", "Sensors/Mittel_S2", "Sensors/Mittel_S3",
          "Sensors/Ring_S1", "Sensors/Ring_S2", "Sensors/Ring_S3",
          "Sensors/Klein_S1", "Sensors/Klein_S2",
        };
        private Quaternion[] fingerRots = new Quaternion[fingerPaths.Length];

        [Header("Values to Read")]

        private float[] bendValues = new float[DMS_Paths.Length];

        private Quaternion wristRotation;
        public bool connecting=true;

        public void rumble(float[] strength, float duration)
        {
            float[] strengthAndDur = new float[strength.Length + 1];
            strength.CopyTo(strengthAndDur, 0);
            strengthAndDur[strength.Length] = duration;
            JToken rumblingData = JToken.FromObject(strengthAndDur);
            Database.Database.Set("Rumble", rumblingData);
        }
        public void rumbleAll(float strength, float duration)
        {
            rumble(new float[] { strength, strength, strength, strength, strength, strength }, duration);

        }
        void Reconnect()
        {
            Database.Reconnect();
            Database.Database.On("Sensors", DataChanged);
            connecting = true;


        }
        private void Start()
        {
            Database.Database.On("Sensors", DataChanged);
            //StartCoroutine(continuedSensorCheck(.1f));
        }
        private void Awake()
        {
            GloveScript.gloveData=this;
        }
        IEnumerator continuedSensorCheck(float timeBetweenChecks)
        {
            checkAllSensors();
            yield return new WaitForSeconds(timeBetweenChecks);
        }
        private void DataChanged(IEnumerable<string> arg1, object arg2)
        {
            connecting = false;
            lastTime = DateTime.Now;
            assignValues();

            foreach (var item in callBacks)

            {
                item(bendValues, armRotation, wristRotation, fingerRots);
            }

        }
        private void Update()
        {
            
            if (!connecting&&DateTime.Now>lastTime.AddSeconds(timeToReconnect))
            {
                Debug.Log("Reconnecting");
                Reconnect();
            }
        }
        void checkSensors()
        {
            bool allZero = true;
            foreach (float item in bendValues)
            {
                if (true)
                {
                    allZero &= item == 0f;
                }
            }
            if (allZero)
            {
                return;
            }
            for (int i = 0; i < bendValues.Length; i++)
            {
                if (broken(bendValues[i]))
                {
                    removeSensor(i);
                }
            }
        }
        void checkAllSensors()
        {
            bendValues = new float[DMS_Paths.Length];

            checkSensors();
        }
        private void removeSensor(int i)
        {
            removeAt(ref bendValues, i);

        }

        private void removeAt(ref float[] values, int index)
        {
            var newValues = new List<float>();
            for (int i = 0; i < values.Length; i++)
            {
                if (i != index)
                {
                    newValues.Add(values[i]);
                }
            }
            values = newValues.ToArray();
        }

        private bool broken(float v)
        {
            return v == 0f;
        }
        public void setDiagnoseValues(float[] angles)
        {
            JObject diagnoseValues = new JObject();
            //Thumb
            diagnoseValues["BW_D1_GG"] = (float)Math.Truncate(angles[1]);
            diagnoseValues["BW_D1_EG"] = (float)Math.Truncate(angles[2]);

            //Index                          
            diagnoseValues["BW_D2_GG"] = (float)Math.Truncate(angles[3]);
            diagnoseValues["BW_D2_MG"] = (float)Math.Truncate(angles[4]);
            diagnoseValues["BW_D2_EG"] = (float)Math.Truncate(angles[5]);

            //Middle                     
            diagnoseValues["BW_D3_GG"] = (float)Math.Truncate(angles[6]);
            diagnoseValues["BW_D3_MG"] = (float)Math.Truncate(angles[7]);
            diagnoseValues["BW_D3_EG"] = (float)Math.Truncate(angles[8]);

            //Ring                       
            diagnoseValues["BW_D4_GG"] = (float)Math.Truncate(angles[9]);
            diagnoseValues["BW_D4_MG"] = (float)Math.Truncate(angles[10]);
            diagnoseValues["BW_D4_EG"] = (float)Math.Truncate(angles[11]);

            //Pinky                      
            diagnoseValues["BW_D5_GG"] = (float)Math.Truncate(angles[12]);
            diagnoseValues["BW_D5_MG"] = (float)Math.Truncate(angles[13]);
            diagnoseValues["BW_D5_EG"] = (float)Math.Truncate(angles[13]);


            Database.Database.Set("Diagnose", diagnoseValues);
        }
        private void assignValues()
        {

            for (int i = 0; i < bendValues.Length; i++)
            {
                var value = Database.Database.Get(DMS_Paths[i]);
                float bendAmount = (float)value["W"];
                bendValues[i] = bendAmount;

            }
            try
            {
                var wristQuat = Database.Database.Get(wristPath);
                var wristRotRaw = new Quaternion((float)wristQuat["X"], (float)wristQuat["Y"], (float)wristQuat["Z"], (float)wristQuat["W"]);
                wristRotation = Quaternion.Euler(90, 0, 0) * wristRotRaw * Quaternion.Inverse(Quaternion.Euler(90, 0, 0));
            }
            catch 
            {

                
            }
            try
            {
                var armQuat = Database.Database.Get(armPath);
                var armRotRaw = new Quaternion((float)armQuat["X"], (float)armQuat["Y"], (float)armQuat["Z"], (float)armQuat["W"]);
                armRotation =
                    Quaternion.Euler(90, 0, 0) *
                    armRotRaw
                    * Quaternion.Inverse(Quaternion.Euler(90, 0, 0))
                    * Quaternion.Euler(0, 0,- 90);
                    ;
            }
            catch { }
            for (int i = 0; i < fingerPaths.Length; i++)
            {
                try
                {

                    var quat = Database.Database.Get(fingerPaths[i]);
                    var rotRaw = new Quaternion((float)quat["X"], (float)quat["Y"], (float)quat["Z"], (float)quat["W"]);
                    fingerRots[i] = Quaternion.Euler(90, 0, 0) * rotRaw * Quaternion.Inverse(Quaternion.Euler(90, 0, 0));
                }

                catch { }

            }

        }


        public void sensorData(HandValuesCallback callback)
        {
            callBacks.Add(callback);
        }

    }



}
