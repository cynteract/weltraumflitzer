﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Database;

public class DatabaseScript : MonoBehaviour {
    public RemoteDatabase Database { get; private set; }
    
    public DatabaseScript()
    {
        Database = new RemoteDatabase();
    }
    public void Reconnect()
    {

        Database.Close();
        Database = new RemoteDatabase();
        Database.HandleAsync();
    }

    // Use this for initialization
    void Start () {
        Database.HandleAsync();
	}

    private void OnDestroy()
    {
        Database.Close();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
