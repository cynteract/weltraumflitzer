﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GloveValues {


    public class calibSC : MonoBehaviour {
        private List<GloveAnglesCallback> callbacks = new List<GloveAnglesCallback>();
        private bool calledBack;
        public Quaternion wristRot, armRot;
        public 
            Quaternion[] fingerRot=new Quaternion[14];

        private float[] rawBendAngles=new float[14];
        public float[] calibAngles = new float[14];
        // Use this for initialization
        void Start () {
            GloveScript.gloveData.sensorData(callback);
        }
        private void Awake()
        {
            GloveScript.calibSC = this;
        }
        public void SubscribeAngles(GloveAnglesCallback gloveAnglesCallback)
        {
            callbacks.Add(gloveAnglesCallback);
        }
        private void callback(float[] values, Quaternion armRotation, Quaternion wristRotation, Quaternion[] fingerRotations)
        {
            calledBack = true;
            wristRot = eulerAndBack(wristRotation);
            armRot = eulerAndBack(armRotation);
            for (int i = 0; i < fingerRot.Length; i++)
            {
                fingerRot[i] = eulerAndBack(fingerRotations[i]);
            }

            setAngles();
            foreach (var item in callbacks)
            {
                item(calibAngles, values);
            }
        }

        private void setAngles()
        {
            for (int i = 0; i < rawBendAngles.Length; i++)
            {
                if (i%3==0)
                {
                    rawBendAngles[i] = AngleArroundAxis(wristRot, fingerRot[i],- Vector3.forward, Vector3.up);
                }
                else
                {
                    rawBendAngles[i] = AngleArroundAxis(fingerRot[i-1], fingerRot[i], Vector3.right, Vector3.up);
                }
                calibAngles[i] = rawBendAngles[i];
            }
        }

        internal bool recievedUpdate()
        {
            return calledBack;
        }

        Quaternion eulerAndBack(Quaternion rot)
        {
            Vector3 meinEuler = rot.eulerAngles;
            return Quaternion.Euler(meinEuler);
        }
        float AngleArroundAxis(Quaternion from, Quaternion to, Vector3 axis, Vector3 up)
        {
            Vector3 projectedUPfrom = Vector3.ProjectOnPlane(from*up, from * axis);
            Vector3 projectedUPTo = Vector3.ProjectOnPlane(to * up, from * axis);
            return Vector3.SignedAngle(projectedUPfrom, projectedUPTo, from * axis);
        }

    }
}