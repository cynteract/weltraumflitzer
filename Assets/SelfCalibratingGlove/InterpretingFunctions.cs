﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GloveValues
{
    public class InterpretingFunctions : ScriptableObject
    {

        public static bool CompareValueToThreashhold(float[] imuAngles, float[] threashholds, compare compare, bool[] fingersToCare)
        {
            bool localClosed = true;
            for (int i = 0; i < imuAngles.Length; i++)
            {
                if (fingersToCare[i])
                {
                    localClosed &= compare(imuAngles[i], threashholds[i]);
                }

            }

            return localClosed;
        }
        public static bool CompareValueToThreashhold(float[] imuAngles, float[] threashholds, compare compare)
        {
            bool localClosed = true;
            for (int i = 0; i < imuAngles.Length; i++)
            {
                localClosed &= compare(imuAngles[i], threashholds[i]);
            }
            return localClosed;
        }
        private static void setFingerAverages(ref float[] fingerAverages, float[] bend)
        {
            for (int i = 0; i < fingerAverages.Length; i++)
            {
                if (i < 4)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        fingerAverages[i] += bend[i * 3 + j];
                    }
                }
                else
                {
                    for (int j = 0; j < 2; j++)
                    {
                        fingerAverages[i] += bend[i * 3 + j];

                    }
                }

            }
        }

        public static bool greater(float a, float b)
        {
            return a > b;
        }
        public static bool smaller(float a, float b)
        {
            return a < b;
        }

        public static void  fingersClosed(ref bool[] fingersClosed, float[] imuAngles,
            float[] middleThreashholds, float[] closedThreashholds, float[] openThreashholds, FingerMeasureMode fingerMeasureMode )
        {
            if (fingerMeasureMode==FingerMeasureMode.Average)
            {
                float[] fingerAverages = new float[5], middleAverages = new float[5];

                setFingerAverages(ref fingerAverages, imuAngles);
                setFingerAverages(ref middleAverages, middleThreashholds);

                for (int i = 0; i < fingersClosed.Length; i++)
                {
                    fingersClosed[i] = fingerAverages[i] > middleAverages[i];
                }
            }
            else if (fingerMeasureMode == FingerMeasureMode.AverageTwoThreashholds)
            {
                float[] fingerAverages = new float[5], closedAverages = new float[5], openAverages = new float[5];

                setFingerAverages(ref fingerAverages, imuAngles);
                setFingerAverages(ref closedAverages, closedThreashholds);
                setFingerAverages(ref openAverages, openThreashholds);
                for (int i = 0; i < fingersClosed.Length; i++)
                {
                    if (!fingersClosed[i])
                    {
                        fingersClosed[i] = fingerAverages[i] > closedAverages[i];
                    }
                    else
                    {
                        fingersClosed[i] =!( fingerAverages[i] < openAverages[i]);
                    }
                }
            }
            
        }
        public static void interprIMUData(ref bool closed, IMUMeasureMode imuMeasureMode, float imuAverage, float[] imuAngles, float[] middleThreashholds, float[] closedThreashholds, float[] openThreashholds, bool[] fingersToCare)
        {
            if (imuMeasureMode == IMUMeasureMode.OneThreashhold)
            {
                if (!closed)//open ;)
                {
                    closed = CompareValueToThreashhold(imuAngles, middleThreashholds, smaller);
                }
                else
                {

                    closed = !CompareValueToThreashhold(imuAngles, middleThreashholds, greater);
                }

            }
            else if (imuMeasureMode == IMUMeasureMode.OpenAndCloseThreashholds)
            {
                if (!closed)//open ;)
                {
                    closed = CompareValueToThreashhold(imuAngles, closedThreashholds, greater);
                }
                else
                {

                    closed = !CompareValueToThreashhold(imuAngles, openThreashholds, smaller);
                }
            }
            else if (imuMeasureMode == IMUMeasureMode.Average)
            {
                float averageMiddle = 0;
                for (int i = 0; i < imuAngles.Length; i++)
                {

                    averageMiddle += middleThreashholds[i];
                }
                closed = imuAverage > averageMiddle ? true : false;
            }
            else if (imuMeasureMode == IMUMeasureMode.AverageTwoThreashholds)
            {
                float averageOpenThreash = 0, averageClosedThreash = 0;
                for (int i = 0; i < imuAngles.Length; i++)
                {

                    averageOpenThreash += openThreashholds[i];
                    averageClosedThreash += closedThreashholds[i];


                }
                if (!closed)//open ;)
                {
                    closed = imuAverage > averageClosedThreash ? true : false;
                }
                else
                {
                    closed = imuAverage < averageOpenThreash ? false : true;
                }

            }
            else if (imuMeasureMode == IMUMeasureMode.FingerAverage)
            {
                float[] fingerAverages = new float[5], middleAverages = new float[5];

                setFingerAverages(ref fingerAverages, imuAngles);
                setFingerAverages(ref middleAverages, middleThreashholds);

                if (!closed)//open ;)
                {
                    closed = CompareValueToThreashhold(fingerAverages, middleAverages, greater, fingersToCare);
                }
                else
                {
                    closed = !CompareValueToThreashhold(fingerAverages, middleAverages, smaller, fingersToCare);

                }

            }
            else if (imuMeasureMode == IMUMeasureMode.FingeraverageTwoThreashholds)
            {
                float[] fingerAverages = new float[5], closedAverages = new float[5], openAverages = new float[5]; ;

                setFingerAverages(ref fingerAverages, imuAngles);
                setFingerAverages(ref closedAverages, closedThreashholds);
                setFingerAverages(ref openAverages, openThreashholds);

                if (!closed)//open ;)
                {
                    closed = CompareValueToThreashhold(fingerAverages, closedAverages, greater, fingersToCare);
                }
                else
                {

                    closed = !CompareValueToThreashhold(fingerAverages, openAverages, smaller, fingersToCare);
                }
            }


        }
        public static void interprForceData(ref bool closed,ForceMeasureMode forceMeasureMode, float forceAverage)
        {
            if (forceMeasureMode == ForceMeasureMode.Average)
            {
                closed = forceAverage > 1000;
            }
            else if (forceMeasureMode == ForceMeasureMode.AverageTwoThreashholds)
            {

            }
            else if (forceMeasureMode == ForceMeasureMode.Finger)
            {

            }
            else if (forceMeasureMode == ForceMeasureMode.FingerTwoThreasHolds)
            {

            }
        }
    }
}