﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Cynteract.PlayerInput
{
    public delegate void KeyAction();
    public delegate bool KeyFunction(KeyCode key);
    public delegate bool KeyActionCondition();
    public struct KeyBind
    {
        public KeyCode key;
        public KeyFunction keyFunction;
        public KeyAction action;
        public KeyActionCondition keyActionCondition;

        public KeyBind(KeyCode key, KeyAction action)
        {
            this.key = key;
            this.action = action;
            this.keyFunction = Input.GetKey;
            keyActionCondition = () => true;
        }

        public KeyBind(KeyCode key, KeyAction action, KeyFunction keyFunction)
        {
            this.key = key;
            this.action = action;
            this.keyFunction = keyFunction;
            keyActionCondition = () => true;
        }
        public KeyBind(KeyCode key, KeyAction action, KeyFunction keyFunction, KeyActionCondition  keyActionCondition)
        {
            this.key = key;
            this.action = action;
            this.keyFunction = keyFunction;
            this.keyActionCondition = keyActionCondition;
        }
    }
    public class PlayerInput : MonoBehaviour
    {

        List<KeyBind> keyBinds = new List<KeyBind>();
        // Use this for initialization
        void Start()
        {

        }
        public void callbackKey(KeyBind keyBind) {
            keyBinds.Add(keyBind);
        }
        public void callbackKey(List<KeyBind> keyBind)
        {
            keyBinds.AddRange(keyBind);
        }
        public void callbackKey( params KeyBind[] bindsToAdd)
        {
            foreach (var item in bindsToAdd)
            {
                keyBinds.Add(item);
            }

        }
        public void stopCallBackKey(KeyBind keyBind)
        {
            try
            {
                keyBinds.Remove(keyBind);
            }
            catch 
            {

                print("Keybind "+keyBind +" is not in List");
            }
        }
        public void stopCallBackKey(params KeyBind[] bindsToAdd)
        {
            foreach (var item in bindsToAdd)
            {
                try
                {
                    keyBinds.Remove(item);
                }
                catch
                {

                    print("Keybind " + item + " is not in List");
                }
            }

        }
        void Awake()
        {
            GlobalValues.playerInput = this;
        }       
        // Update is called once per frame
        void Update()
        {
            try
            {
                foreach (var item in keyBinds)
                {
                    if (item.keyActionCondition() && item.keyFunction(item.key))
                    {
                        item.action();
                    }
                }
            }
            catch 
            {

                print("List was modified");
            }

        }


    }
}
