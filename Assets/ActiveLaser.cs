﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveLaser : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        var hits=Physics2D.RaycastAll(transform.position, transform.right);
        var hitsL=Physics2D.RaycastAll(transform.position, -transform.right);
        foreach (var item in hits)
        {
            if (item.transform.gameObject.GetComponent<Enemy>())
            {
                item.transform.gameObject.GetComponent<Enemy>().ApplyDamage(1);
            }
        }
        foreach (var item in hitsL)
        {
            if (item.transform.gameObject.GetComponent<Enemy>())
            {
                item.transform.gameObject.GetComponent<Enemy>().ApplyDamage(1);
            }
        }
    }
}
