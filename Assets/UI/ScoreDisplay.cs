﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour {
    public Text tm;
    public GameObject[] hearts;
    public GameObject heartsPanel;
    public GameObject heartsPanelGreater3;
    public Text heartsNumber;
    
    private void Update()
    {
        tm.text="Punkte "+(GlobalValues.levelInformation.score);
        int playerHealth = GlobalValues.levelInformation.hp;
        if (playerHealth<=3)
        {
            heartsPanel.SetActive(true);
            heartsPanelGreater3.SetActive(false);
            for (int i = 0; i < hearts.Length; i++)
            {
                hearts[i].SetActive(playerHealth >= i+1);
            }

        }

        else
        {
            for (int i = 0; i < hearts.Length; i++)
            {
                hearts[i].SetActive(true);
            }
            heartsPanel.SetActive(false);
            heartsPanelGreater3.SetActive(true);
            heartsNumber.text = playerHealth.ToString();
        }

    }
}
