﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MainMenu : MonoBehaviour {
     Calibration.Mode calibration;
    bool calibrate = false;
    private void OnDisable()
    {
        if (calibrate)
        {
            GlobalValues.gameControl.Calibrate(calibration);
            calibrate = false;
        }

    }

    public void AttachAction(Calibration.Mode cal)
    {
        calibrate = true;
        calibration = cal;
    }
}
