﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LoadLevels : MonoBehaviour {
    Dropdown dropdown;
    private void Awake()
    {
        dropdown = GetComponent<Dropdown>();
    }
    private void OnEnable()
    {
        List<string> options = new List<string>();
        for (int i = 0; i < GlobalValues.levelSwitcher.levels.Length; i++)
        {
            options.Add((i+1).ToString());
        }
        dropdown.options = new List<Dropdown.OptionData>();
        dropdown.AddOptions(options);
    }

}
