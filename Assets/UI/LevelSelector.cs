﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelector : MonoBehaviour {
    public int levelToLoad;
    public void loadLevel()
    {
        GlobalValues.LoadLevel(levelToLoad);
    }
    public void setLevelToLoad(int level)
    {
        levelToLoad = level;

    }
}
