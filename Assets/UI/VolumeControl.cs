﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeControl : MonoBehaviour {

    public Slider slider;


    public void changeVolume() {
        PlayerPrefs.SetFloat("volume", slider.value);
        AudioListener.volume = slider.value;
    }
    private void Awake()
    {
        print(PlayerPrefs.GetFloat("volume"));
        slider.value=PlayerPrefs.GetFloat("volume");
        AudioListener.volume = PlayerPrefs.GetFloat("volume");
    }
}
