﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cynteract.PlayerInput;
public class MenuActivator : MonoBehaviour
{
    public GameObject skip,levelSelector;
    private void Start()
    {
        KeyBind keyBind = new KeyBind(KeyCode.Escape, toggleMenu, Input.GetKeyDown, notCalibrating);
        GlobalValues.playerInput.callbackKey(keyBind);
    }
    public bool notCalibrating()
    {
        return !GlobalValues.calibrating&&!skip.activeSelf&&!levelSelector.activeSelf;

    }
    public GameObject menu,notMenu;
        public void toggleMenu()
        {
            if (menu.activeSelf)
            {
                menu.SetActive(false);
                notMenu.SetActive(true);
                GlobalValues.timeControl.UnPause();
            }
            else
            {
                menu.SetActive(true);
                notMenu.SetActive(false);
            GlobalValues.timeControl.Pause();
            }


        }
}
