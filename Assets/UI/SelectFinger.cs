﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GloveValues;
using System;

public class SelectFinger : MonoBehaviour {
    public FingerToggle[] toggles;
    public MainMenu mainMenu;
    private void Awake()
    {
        for (int i = 0; i < toggles.Length; i++)
        {
            toggles[i].index = i;

            toggles[i].selectFinger = this;

        }

    }
    private void Start()
    {
        for (int i = 1; i < toggles.Length; i++)
        {
            toggles[i].ScriptedOn(true);


        }
        toggles[0].ScriptedOn(false);
        GloveScript.gloveInterpreter.imuMeasureMode = IMUMeasureMode.AverageTwoThreashholds;
    }
    public void Invert()
    {
        if (toggles[0].on)
        {
            for (int i = 1; i < toggles.Length; i++)
            {
                toggles[i].ScriptedOn(true);
            }
            toggles[0].ScriptedOn(false);
            mainMenu.AttachAction(Calibration.Mode.Fingers);
            GloveScript.gloveInterpreter.imuMeasureMode = IMUMeasureMode.AverageTwoThreashholds;

        }
        else
        {
            for (int i = 1; i < toggles.Length; i++)
            {
                toggles[i].ScriptedOn(false);
            }
            toggles[0].ScriptedOn(true);
            mainMenu.AttachAction(Calibration.Mode.Thumb);
            GloveScript.gloveInterpreter.imuMeasureMode = IMUMeasureMode.FingeraverageTwoThreashholds;
        }

    }
    public void ActivateFinger(int index, bool active)
    {
        bool calibrateThumb=false, calibrateFingers = false;
        toggles[index].ScriptedOn(active);
        if (active)
        {
            if (index == 0)//Thumb
            {
                calibrateThumb = true;
                for (int i = 1; i < toggles.Length; i++)
                {

                    toggles[i].ScriptedOn(false);
                }
            }
            else 
            {
                if (toggles[0].on)
                {
                    calibrateFingers = true;
                }
                toggles[0].ScriptedOn(false);


            }
        }
        bool anySelected = false;
        for (int i = 0; i < toggles.Length; i++)
        {
            anySelected|=toggles[i].on;
        }
        if (anySelected)
        {
            GlobalValues.cannonControl.glove = true;
        }
        else
        {
            GlobalValues.cannonControl.glove = false;
        }
        
        if (calibrateThumb)
        {
            
            mainMenu.AttachAction(Calibration.Mode.Thumb);
        }
        else if (calibrateFingers)
        {
            mainMenu.AttachAction(Calibration.Mode.Fingers);
        }
    }
}
