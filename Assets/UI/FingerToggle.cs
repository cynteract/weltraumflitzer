﻿using GloveValues;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FingerToggle : MonoBehaviour {
    Toggle toggle;
    internal int index;
    internal bool on;
    internal SelectFinger selectFinger;
    private void Awake()
    {
        toggle = GetComponent<Toggle>();
    }
    public void  On(bool v)
    {
        on = v;
        toggle.isOn = v;
        selectFinger.ActivateFinger(index, v);
    }

    
    public void ScriptedOn(bool v) {
        toggle.isOn = v;
        on = v;
        GloveScript.gloveInterpreter.fingersToCare[index] = v;

    }

}
