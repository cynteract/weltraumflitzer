﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GloveValues;
public class Gamecontrol : MonoBehaviour {
    public Calibration calibration;

    private void Start()
    {
        Calibrate(Calibration.Mode.Fingers);
    }

    public void Calibrate(Calibration.Mode mode)
    {
        calibration.gameObject.SetActive(true);
        calibration.Calibrate(mode,() => { });
    }
    public void Calibrate()
    {
        if (GloveScript.gloveInterpreter.fingersToCare[0])
        {
            Calibrate(Calibration.Mode.Thumb);
        }
        else
        {
            Calibrate(Calibration.Mode.Fingers);
        }
    }
    private void Awake()
    {
        GlobalValues.gameControl = this;
    }
}
