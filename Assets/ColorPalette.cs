﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class ColorPalette : ScriptableObject {
    public Color[] colors;
    public Color randomColor()
    {
        int index= UnityEngine.Random.Range(0, colors.Length - 1);
        return colors[index];
    }
}
