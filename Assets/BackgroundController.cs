﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour {

    
    public ColorPalette colorPalette;
    private void Awake()
    {
        GlobalValues.backgroundController = this;
    }

    internal void changeBackground()
    {
        Camera.main.backgroundColor = colorPalette.randomColor();
    }




}
