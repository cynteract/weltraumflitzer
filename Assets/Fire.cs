﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZObjectPools;
using EZCameraShake;
public class Fire : MonoBehaviour {
    public GameObject ball;
    public Transform cannonBallParent;
    public float power;


    internal void Refill()
    {
    }
    internal void Shoot()
    {
        //CameraShaker.Instance.Shake(CameraShakePresets.Shoot);
        GameObject instanceBall;
        instanceBall = Instantiate(ball, transform.position, ball.transform.rotation, cannonBallParent);
        instanceBall.GetComponent<Rigidbody2D>().velocity = power * transform.forward;
    }
}
