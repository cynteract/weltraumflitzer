﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSwitcher : MonoBehaviour {
    public GameObject currentLevel;
    public GameObject[] levels;

    void initLevels()
    {

        var resourceLvls=Resources.LoadAll("levels");
        GameObject[] sortedResourceLevels = new GameObject[resourceLvls.Length];
        for (int i = 0; i < sortedResourceLevels.Length; i++)
        {
            string path = "levels/" +( i+1);
            sortedResourceLevels[i] =(GameObject) Resources.Load(path);
        }
        levels = new GameObject[sortedResourceLevels.Length];
        for (int i = 0; i < resourceLvls.Length; i++)
        {
            levels[i] = sortedResourceLevels[i];
        }
    }
    private void Start()
    {
        GlobalValues.NewLevels();
        GlobalValues.LoadLevel(0);
    }
    private void Awake()
    {
        initLevels();
        GlobalValues.levelSwitcher = this;
    }

    internal void LoadLevel(int level)
    {
        if (level>=levels.Length)
        {
            LoadLevel(UnityEngine.Random.Range(0, levels.Length));
            return;
        }

        try
        {
            Destroy(currentLevel);
        }
        catch 
        {

            Debug.LogWarning("No current Level exists");

        }

        currentLevel = Instantiate(levels[level]);
        GlobalValues.backgroundController.changeBackground();
    }
}
