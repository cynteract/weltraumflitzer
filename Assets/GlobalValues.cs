﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cynteract.PlayerInput;
using UnityEngine;

public class GlobalValues : ScriptableObject
{
    public static LevelInformation levelInformation;

    internal static void ApplyDamage(int v)
    {
        levelInformation.ApplyDamage(v);
    }

    internal static void RestartLevel()
    {
        LoadLevel(levelInformation.currentLevel );
    }

    public static LevelSwitcher levelSwitcher;
    internal static CannonControl cannonControl;
    internal static float maxAngle;
    internal static float minAngle;
    internal static float angle;
    internal static PlayerInput playerInput;
    internal static TimeControll timeControl;
    internal static Gamecontrol gameControl;
    internal static bool calibrating;
    internal static BackgroundController backgroundController;
    internal static Settings settings;
    internal static Spawner spawner;

    public static void NewLevels()
    {
        levelInformation = new LevelInformation();
    }

  
    public  static void NextLevel()
    {
        LoadLevel(levelInformation.currentLevel + 1);
    }
    public  static void LoadLevel(int level)
    {
        //cannonControl.Refill();
        levelInformation.currentLevel = level;
            levelSwitcher.LoadLevel(level);
    }
    private static void Die()
    {
        NewLevels();
        spawner.Delete();
    }
    public class LevelInformation
    {
        public int currentLevel;
        public int score;
        public int hp;
        public LevelInformation()
        {
            score = 0;
            currentLevel = 0;
            hp = 3;
        }

        internal void ApplyDamage(int v)
        {
            hp -= v;
            if (hp<=0)
            {
                GlobalValues.Die();
            }
        }
        public void ScorePoints(int points)
        {
            score += points;
        }
    }


}
