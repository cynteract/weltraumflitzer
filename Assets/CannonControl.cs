﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GloveValues;

public class CannonControl : MonoBehaviour{
    public Fire fire;
    public Transform cannon;
    public Transform testCube;
    public Transform cannonBallpool;
    float delta;
    public Transform left, right;
    public  bool glove;
    private  bool fired;
    private bool frozen;

    private void Awake()
    {
        GlobalValues.cannonControl = this;
    }
    private void Update()
    {
        if (!frozen&&FireAction())
        {
            fire.Shoot();
        }
        if (Input.GetKey("d"))
        {
            delta = Mathf.Clamp01(delta + Time.deltaTime);
        }
        if (Input.GetKey("a"))
        {
            delta = Mathf.Clamp01(delta - Time.deltaTime);
        }
        cannon.position = Vector3.Lerp(left.position, right.position, delta);
    }
    private void Start()
    {
        GloveScript.gloveInterpreter.SubscribeInterpretedData(callback);
    }

    private void callback(bool closed)
    {
        if (glove)
        {
            testCube.rotation = GloveScript.calibSC.armRot;
            Vector3 up = testCube.up, forward = testCube.forward;
            Vector3 uptoRight = Vector3.ProjectOnPlane(up, Vector3.right).normalized;
            Vector3 forwardToRight = Vector3.ProjectOnPlane(forward, Vector3.right).normalized;


            float angleU = Vector3.SignedAngle(Vector3.up, uptoRight, Vector3.right);
            float angleF = Vector3.SignedAngle(Vector3.forward, forwardToRight, Vector3.right);
            delta =Mathf.Clamp01(( angleU * Mathf.Sin(angleF / 180 * Mathf.PI))/90);

            
           
        }
    }
    public void Freeze()
    {
        frozen = true;
    }
    public void Unfreeze()
    {
        frozen = false;
    }
    private  bool FireAction()
    {
        if (glove)
        {
            if (!fired)
            {
                
                bool yes=
                GloveScript.gloveInterpreter.closed;
                fired = yes;
                return yes;
            }
            else
            {
                if (!GloveScript.gloveInterpreter.closed)
                {
                    fired = false;
                }
            }
        }
        return Input.GetKeyDown(KeyCode.Space);
    }

    internal void Refill()
    {
        fire.Refill();
        for (int i = 0; i < cannonBallpool.childCount; i++)
        {
            Destroy(cannonBallpool.GetChild(i).gameObject);
        }
        
    }
}
