﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public Transform left, right;
    static int spawnNumber=5;
    Vector2[] spawnPositions = new Vector2[spawnNumber];
    public List<Vector2> freeSpawnPositions=new List<Vector2>();
    public List<Vector2> occupiedSpawnPositions = new List<Vector2>();
    private bool stopSpawning;

    void SpawnEnemy()
    {
        int delta= UnityEngine.Random.Range(0, freeSpawnPositions.Count);
        print("delta"+delta);
        print("s" + freeSpawnPositions.Count);
        Vector3 position = freeSpawnPositions[delta];
        freeSpawnPositions.Remove(position);
        occupiedSpawnPositions.Add(position);
        StartCoroutine(Release(position)); 
        Instantiate(getItemToSpawn(), position, Quaternion.identity, transform);
    }
    public Transform getItemToSpawn()
    {
        Level currentLevel =

            GlobalValues.settings
            .level;

        List<Transform> objects = new List<Transform>();
        var enemies = currentLevel.CurrentEnemies(GlobalValues.levelInformation.currentLevel);
        for (int i = 0; i < enemies.Length; i++)
        {
            for (int j = 0; j < enemies[i].propability; j++)
            {
                objects.Add(enemies[i].transform);
            }
        }

        return objects[UnityEngine.Random.Range((int)0, objects.Count)];
    }

    internal void Delete()
    {
        for (int i = 2; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    private void Start()
    {
        for (int i = 0; i < spawnNumber; i++)
        {
            spawnPositions[i] = Vector2.Lerp(left.position, right.position, (float)i / (spawnNumber-1));
            freeSpawnPositions.Add(spawnPositions[i]);
        }
        GlobalValues.NewLevels();
        StartCoroutine(SpawnEnemies());
    }
    IEnumerator SpawnEnemies()
    {
        while (!stopSpawning)
        {
            int enemyCount = 1;
                //UnityEngine.Random.Range(0, GlobalValues.settings.level.lvls[GlobalValues.levelInformation.currentLevel].numberOfEnemies);
            for (int i = 0; i < enemyCount; i++)
            {
                SpawnEnemy();
            }
            yield return new WaitForSeconds(GlobalValues.settings.worldGenerationSettings.currentEnemyDelay());
        }
    }
    IEnumerator Release(Vector2 pos)
    {
        yield return new WaitForSeconds(3f);
        occupiedSpawnPositions.Remove(pos);
        freeSpawnPositions.Add(pos);
    }
    private void Awake()
    {
        GlobalValues.spawner = this;
    }
}
