﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour {

    private void Awake()
    {
        GlobalValues.settings = this;
    }
    public WorldGenerationSettings worldGenerationSettings;
    public Level level;
    public bool noDamage;
    public Calibration calibration;
}
