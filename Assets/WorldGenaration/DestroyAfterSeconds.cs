﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class DestroyAfterSeconds : MonoBehaviour
{
    public float secs;
    void Start()
    {
        StartCoroutine(destroy(secs));

    }
    IEnumerator destroy(float secs)
    {

        yield return new WaitForSeconds(secs);
        Destroy(gameObject);
    }
}