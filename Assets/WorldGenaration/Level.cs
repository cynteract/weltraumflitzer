﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
[CreateAssetMenu]
public class Level : ScriptableObject {


    public bool loadFromFolder;
    public Lvl[] lvls;
    [System.Serializable]
    public struct Lvl {
        public int numberOfEnemies;
        public EnemyStruct[] enemies;

    }
    [System.Serializable]
    public struct EnemyStruct
    {
        public Transform transform;
        public int propability;
    }
    public EnemyStruct[] CurrentEnemies(int currentLevel)
    {
        return lvls[currentLevel].enemies;

    }
    /*
    internal void AutoAssingnLevels()
    {
        int[] currentPartCount = new int[lvls.Length];
        for (int i = 0; i < currentPartCount.Length; i++)
        {
            currentPartCount[i] = lvls[i].numberOfEnemies;
        }

        var parts= Resources.LoadAll("Enemies");
        GameObject[] sortedPaths = new GameObject[parts.Length+1];
        for (int i = 0; i < sortedPaths.Length-1; i++)
        {
            string path = (i+1).ToString();
            sortedPaths[i]= (GameObject)Resources.Load(path);
        }
        lvls = new Lvl[sortedPaths.Length];
        for (int i = 0; i < lvls.Length-1; i++)
        {
            lvls[i].additionalEnemies = new Transform[1];
            lvls[i].additionalEnemies[0] = (sortedPaths[i]).transform;
            if (i<currentPartCount.Length)
            {
                lvls[i].numberOfEnemies = currentPartCount[i];
            }
            else if(currentPartCount.Length>0)
            {
                lvls[i].numberOfEnemies = currentPartCount[currentPartCount.Length-1];
            }
            else
            {
                lvls[i].numberOfEnemies = 1;
            }
        }
        lvls[lvls.Length - 1].numberOfEnemies = 4;
    }*/
}
#if UNITY_EDITOR
[CustomEditor(typeof (Level))]
public class LevelEditor : Editor {

    public override void OnInspectorGUI()
    {
        Level level = (Level)target;

        base.OnInspectorGUI();
        /*
        for (int i = 0; i < level.lvls.Length; i++)
        {
            if (level.lvls[i].additionalParts.Length>level.lvls[i].numberOfParts)
            {
                level.lvls[i].numberOfParts = level.lvls[i].additionalParts.Length;
            }
        }*/
        if (level.loadFromFolder)
        {
            if (GUILayout.Button("AutoAssingnLevels"))
            {
                //level.AutoAssingnLevels();
            }
        }

    }
}
#endif

