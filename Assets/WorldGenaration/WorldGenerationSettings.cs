﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu (menuName = "Settings/WorldGenerationSettings")]
public class WorldGenerationSettings : ScriptableObject {
    [Tooltip ("The size of the Tunnel depending on the score divided by divideBy")]
    public AnimationCurve enemyDelayOverScore;

    public int divideBy;
    public float currentEnemyDelay()
    {
        return enemyDelayOverScore.Evaluate(scoreDivided());
    }


    private  float scoreDivided()
    {
        return (float)GlobalValues.levelInformation.score / divideBy;
    }
}
