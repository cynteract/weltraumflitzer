﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveEnemy : Enemy {
    public int damage;
    public List<Enemy> overlappingEnemies;
    protected override void Die()
    {

        SearchEnemies();
        foreach (var item in overlappingEnemies)
        {
            item.ApplyDamage(damage);
        }
        base.Die();

    }
    protected virtual void SearchEnemies()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 10);
        AddColliders(colliders);

    }

    protected virtual void AddColliders(Collider2D[] colliders)
    {
        foreach (var item in colliders)
        {
            var enemy = item.GetComponent<Enemy>();
            if (enemy != null)
            {
                if (!overlappingEnemies.Contains(enemy))
                {
                    overlappingEnemies.Add(enemy);
                }
            }

        }
    }
}
