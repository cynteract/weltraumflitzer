﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;
public class Enemy : MonoBehaviour {
    SpriteRenderer spriteRenderer;
    public int points;
    public Vector2 direction;
    public float speed;
    public int life;
    public Color normalColor, flashyColor;
    public Transform particles;
    private bool canApplyDamage=true;

    // Use this for initialization
    void Start () {
        spriteRenderer.color = normalColor;
	}
    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update () {
        Move();
        Attack();
	}

    protected virtual void Attack()
    {
        if (transform.position.y<-10)
        {
            CameraShaker.Instance.Shake(CameraShakePresets.Bump);
            GlobalValues.ApplyDamage(1);
            Destroy(gameObject);
        }
    }

    public virtual void ApplyDamage(int damage)
    {
        if (canApplyDamage)
        {
            life -= damage;
            spriteRenderer.color = flashyColor;
            CameraShaker.Instance.Shake(CameraShakePresets.Shoot);
            if (life <= 0)
            {
                canApplyDamage = false;
                Die();
            }
            StartCoroutine(ResetColor(spriteRenderer.color, normalColor, .1f, 0.05f));
        }

    }
    IEnumerator ResetColor(Color color, Color to, float timeSpan, float delay)
    {
        yield return new WaitForSeconds(delay);
        Color start=color;
        float startTime=Time.fixedTime;
        print("s" + (startTime + timeSpan));
        print("t" + Time.fixedTime);
        while (Time.fixedTime<startTime+timeSpan)
        {
            color = Color.Lerp(start, to, (Time.fixedTime-startTime) / ( timeSpan));
            spriteRenderer.color = color;
            yield return null;

        }
        spriteRenderer.color = to;
    }
    protected virtual void Die()
    {
        if (particles!=null)
        {
            Instantiate(particles, transform.position, Quaternion.identity);

        }

        CameraShaker.Instance.Shake(CameraShakePresets.Explosion);
        GlobalValues.levelInformation.ScorePoints(points);
        Destroy(gameObject);
    }

    protected virtual void Move()
    {

            transform.position += speed * Time.deltaTime * (Vector3)direction;

        
    }
}
